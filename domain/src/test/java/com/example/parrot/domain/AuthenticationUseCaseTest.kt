package com.example.parrot.domain

import android.content.Context
import com.example.parrot.core.usecase.UseCaseResult
import com.example.parrot.domain.data.user.dto.UserSignUp
import com.example.parrot.domain.usecase.AuthenticationUseCase
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.runners.MockitoJUnitRunner

@RunWith(MockitoJUnitRunner::class)
class AuthenticationUseCaseTest {

    @Mock
    lateinit var mockContext: Context

    lateinit var authUseCase: AuthenticationUseCase

    @Before
    fun setup() {

        val authLocalRepository = AuthenticationLocalRepository()
        val authRemoteRepository = AuthenticationRemoteRepository()
        val authRepository = AuthenticationRepository(authLocalRepository, authRemoteRepository)

        authUseCase = AuthenticationUseCase(authRepository)
    }

    @Test
    fun `signUp with all incorrect fields`() {

        val userTest = UserSignUp()

        runBlocking {

            ((authUseCase.signUp(userTest) as UseCaseResult.Error).cause
                    as AuthenticationUseCase.SignUpValidationException).fieldsError.apply {

                assert (firstOrNull { it.field == AuthenticationUseCase.UserFieldsError.NAME } != null)
                assert (firstOrNull { it.field == AuthenticationUseCase.UserFieldsError.USERNAME } != null)
                assert (firstOrNull { it.field == AuthenticationUseCase.UserFieldsError.EMAIL } != null)
                assert (firstOrNull { it.field == AuthenticationUseCase.UserFieldsError.PASSWORD } != null)
            }
        }
    }

}