package com.example.parrot.domain

import com.example.parrot.domain.util.isEmailValid
import com.example.parrot.domain.util.isUsernameValid
import org.junit.Test

class ValidationUtilsTest {

    @Test
    fun `test emails`() {

        // valid
        assert("daniel@gmail.com".isEmailValid())
        assert("daniel_asd..@gmail.com".isEmailValid())
        assert("Dannnill1...1@gmail.com.br".isEmailValid())
        assert("daniel@teste.br".isEmailValid())
        assert("daniel__ieie@bol.com".isEmailValid())

        // invalid
        assert(!"danielgmail.com".isEmailValid())
        assert(!"daniel@gmail..com".isEmailValid())
        assert(!"daniel@_gmail.com_br".isEmailValid())
        assert(!"daniel@_gmail.com".isEmailValid())
        assert(!"daniel@gmail_com".isEmailValid())

    }

    @Test
    fun `test username`() {

        // valid
        assert("daniel123".isUsernameValid())
        assert("danielfalcao".isUsernameValid())
        assert("dan".isUsernameValid())
        assert("testeUsername".isUsernameValid())
        assert("lele".isUsernameValid())

        // invalid
        assert(!"".isUsernameValid())
        assert(!"us".isUsernameValid())

    }
}