package com.example.parrot.domain.api

import com.example.parrot.domain.BuildConfig
import com.example.parrot.domain.Result
import com.example.parrot.domain.api.RemoteException.Companion.toRemoteException
import com.facebook.stetho.okhttp3.StethoInterceptor
import kotlinx.coroutines.*
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

abstract class RemoteDataSource : CoroutineScope by CoroutineScope(Dispatchers.IO) {

    companion object {

        val okHttpClient: OkHttpClient = let {

            val loggingLevel = HttpLoggingInterceptor.Level.BODY

            OkHttpClient.Builder().apply {

                connectTimeout(60, TimeUnit.SECONDS)
                writeTimeout(60, TimeUnit.SECONDS)
                readTimeout(60, TimeUnit.SECONDS)

//              addInterceptor(HeadersInterceptor())

                if (BuildConfig.DEBUG) {

                    val loggingInterceptor = HttpLoggingInterceptor()
                    loggingInterceptor.level = loggingLevel

                    addInterceptor(loggingInterceptor)
                    addNetworkInterceptor(StethoInterceptor())
                }
            }.build()
        }
    }

    val requestMap: HashMap<String, Job> = hashMapOf()

    fun getRetrofitBuilder(url: String = Paths.BASE_URL): Retrofit {

        return Retrofit.Builder().apply {
            baseUrl(url)
            addConverterFactory(GsonConverterFactory.create())
            client(okHttpClient)
        }.build()
    }

    @Suppress("UNCHECKED_CAST")
    suspend fun <API, T> executeRequest(api: API, avoidLogout: Boolean = false, request: suspend API.() -> T): Result<T> {

        val requestKey = request.toString()
        var repositoryResult: Result<T>? = null

        requestMap[requestKey]?.apply { if (isActive) cancelAndJoin() }
        requestMap[requestKey]

        coroutineScope {

            launch {

                repositoryResult = try {
                    Result.Success(api.request())
                } catch (e: Exception) {

                    val remoteException = e.toRemoteException().apply {
                        this.avoidLogout = avoidLogout
                    }
                    Result.Failure(remoteException)
                }
            }.join()
        }

        return repositoryResult!!
    }

}