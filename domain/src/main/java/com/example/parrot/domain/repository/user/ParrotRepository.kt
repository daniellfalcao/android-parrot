package com.example.parrot.domain.repository.user

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers

abstract class ParrotRepository : CoroutineScope by CoroutineScope(Dispatchers.IO)