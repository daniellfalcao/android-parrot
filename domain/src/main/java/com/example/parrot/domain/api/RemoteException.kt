package com.example.parrot.domain.api

import com.example.parrot.core.exception.ParrotException
import com.example.parrot.domain.R
import com.falcon.turing.core.components.StringWrapper
import com.falcon.turing.core.components.toStringWrapper
import com.google.gson.Gson
import retrofit2.HttpException
import java.net.*

class RemoteException : ParrotException {

    /**
     *  Códigos de erro:
     *
     *  #CLIENT - Um erro na faixa de 400 .. 499
     *  #SERVER - Um erro na faixa de 500 .. 599
     *
     *  #400/BAD_REQUEST - O request é invalido ou não pode ser servido. Geralmente o JSON pode não ser
     *  válido.
     *  #401/UNAUTHORIZED - A requisição requer autenticação do usuário.
     *  #403/FORBIDDEN - O servidor entende a requisição mas o acesso não está liberado.
     *  #404/NOT_FOUND - Não foi encontrado o que se procura.
     *
     *  #TIMEOUT - Esgotou o tempo limite da requisição.
     *  #UNKNOWN_HOST - O endereço IP do host não foi encontrado, acontece também quando não está
     *  conectado a internet.
     *  #CONNECTION - Sem conexão com a internet.
     *  #CANCELED - A requisição foi cancelada, normalmente o usuário recebeu UNAUTHORIZED em outra
     *  requisição paralela a essa.
     *
     *  #UNEXPECTED - Um erro inesperado.
     *
     * */
    enum class RemoteExceptionCode(val message: StringWrapper) {
        CLIENT(StringWrapper(R.string.network_error_try_again)),
        SERVER(StringWrapper(R.string.network_error_server)),
        BAD_REQUEST(StringWrapper(R.string.network_error_bad_request)),
        UNAUTHORIZED(StringWrapper(R.string.network_error_unauthorized)),
        FORBIDDEN(StringWrapper(R.string.network_error_try_again)),
        NOT_FOUND(StringWrapper(R.string.network_error_not_found)),
        TIMEOUT(StringWrapper(R.string.network_error_try_again)),
        UNKNOWN_HOST(StringWrapper(R.string.network_error_connection)),
        CONNECTION(StringWrapper(R.string.network_error_connection)),
        CANCELED(StringWrapper(R.string.network_error_canceled)),
        UNEXPECTED(StringWrapper(R.string.network_error_try_again))
    }

    /**
     * Classe de dados da resposta de erro do serviço
     *
     * */
    data class RemoteError(val mensagem: String)

    companion object {

        fun Throwable.toRemoteException() =
            RemoteException(message, this)

        fun Throwable.toRemoteExceptionCode(): RemoteExceptionCode {

            return when (this) {

                is HttpException -> {
                    when (code()) {
                        HttpURLConnection.HTTP_BAD_REQUEST -> RemoteExceptionCode.BAD_REQUEST
                        HttpURLConnection.HTTP_UNAUTHORIZED -> RemoteExceptionCode.UNAUTHORIZED
                        HttpURLConnection.HTTP_FORBIDDEN -> RemoteExceptionCode.FORBIDDEN
                        HttpURLConnection.HTTP_NOT_FOUND -> RemoteExceptionCode.NOT_FOUND
                        in 400..499 -> RemoteExceptionCode.CLIENT
                        in 500..599 -> RemoteExceptionCode.SERVER
                        else -> RemoteExceptionCode.UNEXPECTED
                    }
                }
                is SocketTimeoutException -> RemoteExceptionCode.TIMEOUT
                is UnknownHostException -> RemoteExceptionCode.UNKNOWN_HOST
                is ConnectException -> RemoteExceptionCode.CONNECTION
                is SocketException -> RemoteExceptionCode.CANCELED
                else -> RemoteExceptionCode.UNEXPECTED
            }
        }
    }

    override var errorMessage: StringWrapper?

    var errorCode: RemoteExceptionCode
        private set

    var avoidLogout = false

    constructor() : super()
    constructor(message: String?, cause: Throwable?) : super(message, cause)

    init {

        // carrega o erro code a partir da error que causou a exception
        errorCode = cause?.toRemoteExceptionCode() ?: RemoteExceptionCode.UNEXPECTED

        // recupera uma mensagem de erro a partir do serviço ou do errorCode
        errorMessage = getErrorMessage()

        handleCode()
    }


    @JvmName("getErrorMessage2")
    private fun getErrorMessage(): StringWrapper {

        return if (cause is HttpException) {
            try {
                Gson().fromJson(cause.response()?.errorBody()?.string(), RemoteError::class.java).mensagem.toStringWrapper()
            } catch (e: Exception) {
                errorCode.message
            }
        } else {
            errorCode.message
        }
    }

    private fun handleCode() {

        if (errorCode == RemoteExceptionCode.UNAUTHORIZED && !avoidLogout) {
            // TODO limpa o banco do aplicativo e manda pra tela inicial
        }
    }

}