package com.example.parrot.domain.util


internal fun String.isNameValid(): Boolean {
    return isNotBlank()
}

internal fun String.isUsernameValid(): Boolean {
    return isNotBlank() && length >= 3
}

internal fun String.isEmailValid(): Boolean {
    return "[a-zA-Z0-9+._%\\-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}(\\.[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25})+".toRegex().matches(this)
}

internal fun String.isPasswordValid(): Boolean {
    return isNotBlank() && length == 8
}

