package com.example.parrot.domain.repository.user

import com.example.parrot.data.user.dto.User
import com.example.parrot.data.user.entity.FriendEntity
import com.example.parrot.data.user.entity.UserEntity
import com.example.parrot.data.user.entity.UserSession
import com.example.parrot.domain.Result
import com.example.parrot.domain.api.RemoteDataSource

class UserRemoteDataSource : RemoteDataSource(), UserDataSource {

    private val API by lazy { getRetrofitBuilder().create(UserAPI::class.java) }

    override suspend fun getUser(email: String, password: String): Result<User> {
        return executeRequest(API, true) { login(User(email = email, password = password)) }
    }

    override suspend fun getUser(id: Int): Result<UserEntity> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun getCurrentUserSession(): Result<UserSession> {
        TODO("Not required for the remote data source.")
    }

    override suspend fun saveUser(user: UserEntity): Result<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun saveUserProfile(user: UserEntity, friends: FriendEntity): Result<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun saveUserSession(userSession: UserSession): Result<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun deleteCurrentUserSession(): Result<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}