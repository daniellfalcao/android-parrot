//package com.example.parrot.domain.usecase
//
//import android.os.RemoteException
//import com.example.parrot.core.exception.ParrotException
//import com.example.parrot.core.repository.RepositoryResult
//import com.example.parrot.core.usecase.UseCase
//import com.example.parrot.core.usecase.UseCaseResult
//import com.example.parrot.domain.data.user.dto.UserSignUp
//import com.falcon.turing.core.components.StringWrapper
//import com.falcon.turing.core.components.toStringWrapper
//
//class AuthenticationUseCase(val authRepository: AuthenticationRepository) : UseCase() {
//
//    enum class UserFieldsError { NAME, USERNAME, EMAIL, PASSWORD, PASSWORD_CONFIRMATION }
//
//    class SignUpValidationException(override var errorMessage: StringWrapper?) : ParrotException() {
//
//        class FieldError(val field: UserFieldsError, error: StringWrapper)
//
//        var fieldsError: MutableList<FieldError> = mutableListOf()
//            private set
//
//        fun addFieldWithErro(fieldError: FieldError) = apply { fieldsError.add(fieldError) }
//
//    }
//
//
//    @Suppress("ThrowableNotThrown")
//    suspend fun signUp(userSignUp: UserSignUp): UseCaseResult<*> {
//
//        val exception = SignUpValidationException(null)
//        var hasException = false
//
//        // validate if name is valid
//        if (!userSignUp.name.isNameValid()) {
//            hasException = true
//            exception.addFieldWithErro(SignUpValidationException.FieldError(UserFieldsError.NAME, "mensagem de erro".toStringWrapper()))
//        }
//
//        // validate if email is valid
//        if (!userSignUp.email.isEmailValid()) {
//            hasException = true
//            exception.addFieldWithErro(SignUpValidationException.FieldError(UserFieldsError.EMAIL, "mensagem de erro".toStringWrapper()))
//        }
//
//        // validade if username is valid
//        if (!userSignUp.username.isUsernameValid()) {
//            hasException = true
//            exception.addFieldWithErro(SignUpValidationException.FieldError(UserFieldsError.USERNAME, "mensagem de erro".toStringWrapper()))
//        }
//
//        // validate if password is valid
//        if (!userSignUp.password.isPasswordValid()) {
//            hasException = true
//            exception.addFieldWithErro(SignUpValidationException.FieldError(UserFieldsError.PASSWORD, "mensagem de erro".toStringWrapper()))
//        }
//
//        // validate if the password and confirmation match
//        if (userSignUp.password.isPasswordValid()
//            && userSignUp.password != userSignUp.passwordConfirmation
//        ) {
//            hasException = true
//            exception.addFieldWithErro(SignUpValidationException.FieldError(UserFieldsError.PASSWORD_CONFIRMATION, "mensagem de erro".toStringWrapper()))
//        }
//
//        return if (hasException) {
//
//            UseCaseResult.Error<SignUpValidationException>(exception)
//
//        } else {
//
//            when (val result = authRepository.signUp(userSignUp)) {
//                is RepositoryResult.Success -> {
//                    UseCaseResult.Success(userSignUp)
//                }
//                is RepositoryResult.Error -> {
//                    UseCaseResult.Error<RemoteException>(result.error)
//                }
//            }
//        }
//    }
//
//    suspend fun signIn(email: String, password: String): UseCaseResult<*> {
//        TODO("")
//    }
//
//    suspend fun signOut(): UseCaseResult<*> {
//        TODO("")
//    }
//
//}