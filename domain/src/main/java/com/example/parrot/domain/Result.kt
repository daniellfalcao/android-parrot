package com.example.parrot.domain

sealed class Result<T> {

    class Success<T>(val data: T) : Result<T>()
    class Failure<T>(val error: Throwable) : Result<T>()
}