package com.example.parrot.domain.repository.user

import com.example.parrot.data.user.dto.User
import retrofit2.http.Body
import retrofit2.http.POST


interface UserAPI {

    @POST("usuario/login")
    suspend fun login(@Body usuario: User): User

}