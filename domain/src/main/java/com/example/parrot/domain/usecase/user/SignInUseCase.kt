package com.example.parrot.domain.usecase.user

import com.example.parrot.core.exception.ParrotException
import com.example.parrot.domain.Result
import com.example.parrot.domain.repository.user.IUserRepository
import com.example.parrot.domain.usecase.CompletableUseCase
import com.example.parrot.domain.util.isEmailValid
import com.example.parrot.domain.util.isPasswordValid
import com.falcon.turing.core.components.StringWrapper
import kotlinx.coroutines.withContext


class SignInUseCase(
    private val userRepository: IUserRepository
) : CompletableUseCase<SignInUseCase.Param, Result<*>>() {

    override suspend fun execute(param: Param): Result<*> {

        val errors = mutableListOf<SignInValidationException.SignInError>()

        if (!param.email.isEmailValid()) {
            errors.add(SignInValidationException.SignInError.EMAIL)
        }

        if (!param.password.isPasswordValid()) {
            errors.add(SignInValidationException.SignInError.PASSWORD)
        }

        if (errors.isNotEmpty()) {
            return Result.Failure<Nothing>(SignInValidationException(errors))
        }

        return withContext(coroutineContext) {
            userRepository.signIn(param.email, param.password)
        }
    }

    class Param(val email: String, val password: String)

    class SignInValidationException(val errors: MutableList<SignInError>) : ParrotException() {
        override var errorMessage: StringWrapper? = null
        enum class SignInError { EMAIL, PASSWORD }
    }
}
