package com.example.parrot.domain.repository.user.mapper

import com.example.parrot.data.user.dto.User
import com.example.parrot.data.user.entity.*

fun User.toUserEntity(): UserEntity {

    return userEntity {
        id = this@toUserEntity.id ?: -1
        name = this@toUserEntity.name ?: ""
        username = this@toUserEntity.username ?: ""
        email = this@toUserEntity.email ?: ""
        photoURL = this@toUserEntity.photoURL ?: ""
    }
}

fun User.toFriendEntity(): FriendEntity {

    return friendEntity {
        id = this@toFriendEntity.id ?: -1
        name = this@toFriendEntity.name ?: ""
        username = this@toFriendEntity.username ?: ""
        email = this@toFriendEntity.email ?: ""
        photoURL = this@toFriendEntity.photoURL ?: ""
    }
}

fun User.toAuthorEntity(): AuthorEntity {

    return authorEntity {
        id = this@toAuthorEntity.id ?: -1
        name = this@toAuthorEntity.name ?: ""
        username = this@toAuthorEntity.username ?: ""
        email = this@toAuthorEntity.email ?: ""
        photoURL = this@toAuthorEntity.photoURL ?: ""
    }
}