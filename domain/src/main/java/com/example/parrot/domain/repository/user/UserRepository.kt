package com.example.parrot.domain.repository.user

import com.example.parrot.data.user.entity.UserEntity
import com.example.parrot.data.user.entity.UserSession
import com.example.parrot.domain.Result
import com.example.parrot.domain.Result.*
import com.example.parrot.domain.api.RemoteException
import com.example.parrot.domain.repository.user.mapper.toUserEntity
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class UserRepository(
    private val local: UserLocalDataSource,
    private val remote: UserRemoteDataSource
) : ParrotRepository(), IUserRepository {

    override suspend fun signIn(email: String, password: String): Result<UserEntity> {

        return when (val result = remote.getUser(email, password)) {

            is Success -> {

                val token = result.data.token ?: ""
                val userEntity = result.data.toUserEntity()
                val userSession = UserSession(token, userEntity.id)

                local.saveUser(userEntity)
                local.saveUserSession(userSession)

                Success(userEntity)
            }

            is Failure -> {
                Failure(result.error)
            }
        }
    }

    override suspend fun signOut() {
        remote.deleteCurrentUserSession()
        delay(2000)
        local.deleteCurrentUserSession()
    }

}