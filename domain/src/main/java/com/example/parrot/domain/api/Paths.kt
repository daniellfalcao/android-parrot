package com.example.parrot.domain.api

import com.example.parrot.domain.BuildConfig

object Paths {

    private const val BASE_URL_DEBUG = "http://172.18.9.240:3010/"
    private const val BASE_URL_RELEASE = ""

    val BASE_URL = if (BuildConfig.DEBUG) BASE_URL_DEBUG else BASE_URL_RELEASE

}