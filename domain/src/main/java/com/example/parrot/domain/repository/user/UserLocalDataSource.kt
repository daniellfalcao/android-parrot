package com.example.parrot.domain.repository.user

import com.example.parrot.data.user.dao.SessionDAO
import com.example.parrot.data.user.dao.UserDAO
import com.example.parrot.data.user.entity.FriendEntity
import com.example.parrot.data.user.entity.UserEntity
import com.example.parrot.data.user.entity.UserSession
import com.example.parrot.domain.Result

class UserLocalDataSource(
    private val userDAO: UserDAO,
    private val sessionDAO: SessionDAO
) : UserDataSource {

    override suspend fun getUser(email: String, password: String): Result<UserSession> {
        TODO("Not required for the local data source.")
    }

    override suspend fun getUser(id: Int): Result<UserEntity> {
        return Result.Success(userDAO.get(id))
    }

    override suspend fun getCurrentUserSession(): Result<UserSession> {
        return Result.Success(sessionDAO.get())
    }

    override suspend fun saveUser(user: UserEntity): Result<*> {
        return Result.Success(userDAO.add(user))
    }

    override suspend fun saveUserProfile(user: UserEntity, friends: FriendEntity): Result<*> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun saveUserSession(userSession: UserSession): Result<*> {
        return Result.Success(sessionDAO.add(userSession))
    }

    override suspend fun deleteCurrentUserSession(): Result<*> {
        return Result.Success(sessionDAO.delete())
    }


}