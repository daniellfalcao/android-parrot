package com.example.parrot.domain.repository.user

import com.example.parrot.data.user.entity.UserEntity
import com.example.parrot.domain.Result


interface IUserRepository {

    suspend fun signIn(email: String, password: String): Result<UserEntity>
    suspend fun signOut()
}