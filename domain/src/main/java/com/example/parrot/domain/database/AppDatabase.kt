package com.example.parrot.domain.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.example.parrot.data.user.dao.SessionDAO
import com.example.parrot.data.user.dao.UserDAO
import com.example.parrot.data.user.entity.*

@Database(
    version = 1 ,
    exportSchema = false,
    entities = [
        AuthorEntity::class,
        FriendEntity::class,
        UserEntity::class,
        UserFriendEntity::class,
        UserSession::class
    ]
)
//@TypeConverters(Converters::class)
abstract class AppDatabase : RoomDatabase() {

    companion object {

        private const val DATABASE_NAME = "parrot"

        private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {

            return instance
                ?: synchronized(this) {
                    instance ?: synchronized(this) {
                        buildDatabase(
                            context
                        )
                    }
                }
        }

        private fun buildDatabase(context: Context): AppDatabase {

            return Room.databaseBuilder(
                context, AppDatabase::class.java,
                DATABASE_NAME
            )
                .addCallback(object : RoomDatabase.Callback() {
                    override fun onCreate(db: SupportSQLiteDatabase) {
                        super.onCreate(db)
                        // faz alguma coisa quando o banco for criado
                    }
                })
                .fallbackToDestructiveMigration()
                .build()
        }

    }

    abstract val sessionDAO: SessionDAO
    abstract val userDAO: UserDAO

}