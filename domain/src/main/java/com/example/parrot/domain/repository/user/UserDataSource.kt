package com.example.parrot.domain.repository.user

import com.example.parrot.data.user.entity.FriendEntity
import com.example.parrot.data.user.entity.UserEntity
import com.example.parrot.data.user.entity.UserSession
import com.example.parrot.domain.Result

interface UserDataSource {

    suspend fun getUser(email: String, password: String): Result<*>

    suspend fun getUser(id: Int): Result<*>


    suspend fun getCurrentUserSession(): Result<*>


    suspend fun saveUser(user: UserEntity): Result<*>

    suspend fun saveUserProfile(user: UserEntity, friends: FriendEntity): Result<*>

    suspend fun saveUserSession(userSession: UserSession): Result<*>



    suspend fun deleteCurrentUserSession(): Result<*>

}