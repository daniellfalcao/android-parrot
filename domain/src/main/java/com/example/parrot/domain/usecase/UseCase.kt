package com.example.parrot.domain.usecase

import com.example.parrot.domain.Result
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.channels.consumeEach

abstract class ObservableUseCase<in Param, T>: CoroutineScope by CoroutineScope(Dispatchers.IO) {
    val channel = Channel<T>()
    abstract suspend fun execute(param: Param): Channel<T>
}

suspend fun<T> Channel<Result<T>>.onResult(event: (Result<T>) -> Unit) {
    consumeEach { event(it) }
}

abstract class CompletableUseCase<in Param, T>: CoroutineScope by CoroutineScope(Dispatchers.IO) {
    abstract suspend fun execute(param: Param): T
}