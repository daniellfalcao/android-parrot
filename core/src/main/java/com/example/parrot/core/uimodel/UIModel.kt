package com.example.parrot.core.uimodel

import android.content.Context
import android.os.Bundle
import androidx.databinding.BaseObservable
import androidx.databinding.ViewDataBinding
import com.example.parrot.core.feedback.SnackbarFeedback
import com.falcon.turing.core.components.StringWrapper

abstract class UIModel<T : ViewDataBinding>(val binding: T, protected val context: Context) : BaseObservable() {

    abstract fun setup()

    open fun showError(error: StringWrapper) {
    }

    open fun saveState(bundle: Bundle) {
        // salva os dados no bundle
    }

    open fun restoreState(bundle: Bundle) {
        // restaura os dados do bundle
    }

}
