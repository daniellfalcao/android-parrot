package com.example.parrot.core.exception

import com.falcon.turing.core.components.StringWrapper

abstract class ParrotException : Exception {

    constructor(message: String?, cause: Throwable?) : super(message, cause)
    constructor(message: String?) : super(message)
    constructor(cause: Throwable?) : super(cause)
    constructor() : super()
    constructor(message: String?, cause: Throwable?, enableSuppression: Boolean, writableStackTrace: Boolean) : super(message, cause, enableSuppression, writableStackTrace)

    abstract var errorMessage: StringWrapper?
}