package com.example.parrot.core.feedback.manager

import android.app.Application
import com.example.parrot.core.feedback.Feedback
import com.example.parrot.core.utils.getActivity
import com.example.parrot.core.view.activity.isAtLeastResumed
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import timber.log.Timber
import java.lang.ref.WeakReference

class FeedbackManager(private val application: Application) : CoroutineScope by CoroutineScope(Dispatchers.Default) {

    companion object {

        const val TAG = "FeedbackManager"

        private var instance: FeedbackManager? = null

        /**
         * Recupera a instância do FeedbackManager
         *
         * */
        fun getInstance(application: Application): FeedbackManager {
            return instance ?: run {
                instance = FeedbackManager(application)
                instance!!
            }
        }

        /**
         * Recupera a instância do FeedbackManager caso já tenha sido criado
         * */
        internal fun getInstance() = instance

    }

    /**
     * Feedbacks que já foram exibidos ou estão sendo exibidos, são usados para checar se um novo
     * feedback vai poder ser exibido ou não.
     *
     * @see canDisplay
     *
     * */
    private val handledFeedbacks = HashMap<String, MutableList<WeakReference<Feedback>>>()

    /**
     * Tenta mostrar um feedback. Para mostrar, verifica primeiro se o feedback pode ser exibido
     * @see canDisplay
     *
     * @param feedback o feedback que deve ser exibido
     *
     * */
    fun showFeedback(feedback: Feedback) {

        if (canDisplay(feedback)) {

            // adiciona o feedback ao map de feedbacks que já foram tratados
            handledFeedbacks[feedback.feedbackScreenKey()]?.add(WeakReference(feedback)) ?: run {
                handledFeedbacks[feedback.feedbackScreenKey()] = mutableListOf<WeakReference<Feedback>>().apply {
                    add(WeakReference(feedback))
                }
            }

            // informa ao feedback que ele foi tratado
            feedback.hasBeenHandled = true

            // exibe na tela o feedback
            feedback.show()
        }
    }

    /**
     * Descarta todos os feedbacks que tenha o mesmo screenKey
     *
     * @param screenKey chave para apagar os feedbacks
     *
     * */
    // TODO: falta chamar na application
    fun discardFeedbacks(screenKey: String) {

        // dismiss todos os feedback que ainda podem estar sendo exibidos
        handledFeedbacks[screenKey]?.apply { forEach { it.get()?.dismiss() } }

        // descarta a screenKey do map de feedbacks
        handledFeedbacks.remove(screenKey)
    }


    /**
     * Verifica se um feedback pode ser exibido na tela dependendo do estado da activity e
     * verificando se um mesmo feedback já está sendo exibido.
     *
     * @param feedback é o feedback que vai ser analisado
     *
     * */
    private fun canDisplay(feedback: Feedback): Boolean {

        // verifica se o ciclo de vida da activity que vai exibir o feedback permite mostrar o feedback
        if (feedback.view?.getActivity()?.isAtLeastResumed() == false) {
            return false
        }

        // verifica se algum feedback dessa activity que possa ter o mesmo ID ou mensagem está sendo exibido atualmente
        // se estiver sendo exibido retorna false para que não sejá exibido outro feedback igual ao que já está sendo exibido
        // se não estiver sendo exibido no final da funcao retorna true para que o feedback seja exibido
        handledFeedbacks[feedback.feedbackScreenKey()]?.let {

            it.forEach { weakReferenceFeedback ->

                weakReferenceFeedback.get()?.let { handledFeedback ->

                    Timber.tag(TAG).d("O feedback tem o mesmo ID: ${handledFeedback.id != 0 && handledFeedback.id == feedback.id}")
                    Timber.tag(TAG).d("O feedback tem a mesma mensagem: ${handledFeedback.message?.invoke(application) == feedback.message?.invoke(application)}")
                    Timber.tag(TAG).d("O feedback está sendo mostrado atualmente: ${handledFeedback.isShowing()}")

                    if (((handledFeedback.id != 0 && handledFeedback.id == feedback.id)
                                || handledFeedback.message?.invoke(application) == feedback.message?.invoke(application))
                        && handledFeedback.isShowing()
                    ) {
                        return false
                    }
                }
            }
        }

        return true
    }

}

/**
 * Extension function para exibir um feedback sem ter que recuperar a instancia do
 * FeedbackManager, caso a instancia ainda não tenha sido criada, o feedback não será
 * exibido.
 *
 * */
fun <T : Feedback> T.dispatchToManager(): T {
    FeedbackManager.getInstance()?.showFeedback(this)
    return this
}


