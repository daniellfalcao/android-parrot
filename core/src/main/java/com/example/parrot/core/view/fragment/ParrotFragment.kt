package com.example.parrot.core.view.fragment

import com.example.parrot.core.view.activity.ParrotActivity
import com.falcon.turing.core.fragment.TFragment

abstract class ParrotFragment : TFragment() {

    override var eventBusEnabled: Boolean = false
}

fun ParrotFragment.isConnectedToInternet() = (activity as? ParrotActivity)?.isConnectedToInternet