package com.example.parrot.core.interceptor

import android.os.Build
import com.example.parrot.core.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import java.io.IOException


/**
 * Interceptor responsável por popular todos os headers padrões
 */
class ParrotInterceptor : Interceptor {

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {

        val request = chain.request().newBuilder()
            .header("Content-Type", "application/json")
            .addHeader("Platform", "Android")
            .addHeader("SO-Version", Build.VERSION.SDK_INT.toString())
            .addHeader("Connection", "close")
            .addHeader("App-Version", BuildConfig.VERSION_NAME)
            .addHeader("App-Build", BuildConfig.VERSION_CODE.toString())
            .addHeader("Device-Model", Build.MANUFACTURER + " " + Build.MODEL).build()

        return chain.proceed(request)
    }
}