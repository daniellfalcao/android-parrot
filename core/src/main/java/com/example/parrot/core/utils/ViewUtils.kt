package com.example.parrot.core.utils

import android.view.View
import com.example.parrot.core.view.activity.ParrotActivity

fun View.getActivity(): ParrotActivity? = context as? ParrotActivity?