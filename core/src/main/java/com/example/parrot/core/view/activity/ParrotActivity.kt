package com.example.parrot.core.view.activity

import android.os.Bundle
import androidx.lifecycle.Lifecycle
import com.falcon.turing.core.activity.TActivity
import com.novoda.merlin.Merlin
import com.novoda.merlin.MerlinsBeard

abstract class ParrotActivity : TActivity() {

    companion object {
        fun ParrotActivity.screenKey() = localClassName.toString()
    }

    override var eventBusEnabled: Boolean = false

    private var merlin: Merlin? = null
    private var merlinsBeard: MerlinsBeard? = null

    var isConnectedToInternet = false
        get() { updateInternetConnectionState(); return field }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        merlinsBeard = MerlinsBeard.Builder().build(this)
        merlin = Merlin.Builder().withAllCallbacks().build(this).apply {

            registerConnectable {
                isConnectedToInternet = true
            }

            registerDisconnectable {
                isConnectedToInternet = false
            }
        }
    }

    override fun onResume() {
        super.onResume()

        merlin?.bind()
        updateInternetConnectionState()
    }

    override fun onPause() {
        super.onPause()

        merlin?.unbind()
    }

    private fun updateInternetConnectionState() {
        merlinsBeard?.hasInternetAccess { isConnectedToInternet = it }
    }

}

fun ParrotActivity.isAtLeastResumed() = lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)