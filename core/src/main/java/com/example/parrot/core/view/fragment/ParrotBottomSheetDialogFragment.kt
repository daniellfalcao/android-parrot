package com.example.parrot.core.view.fragment

import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import android.widget.FrameLayout
import com.example.parrot.core.R
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

abstract class ParrotBottomSheetDialogFragment : BottomSheetDialogFragment() {

    private var _onVisibleChanged: (isVisibleToUser: Boolean) -> Unit = { }
    private var _onStateChanged: (newState: Int) -> Unit = { }
    private var _onSlide: (slideOffset: Float) -> Unit = { }

    protected open var initialState: Int = BottomSheetBehavior.STATE_EXPANDED
    protected open var shouldSkipCollapsedState: Boolean = false
    protected open var enableDim: Boolean = true

    fun setOnVisibleChanged(event: (isVisibleToUser: Boolean) -> Unit) = apply { _onVisibleChanged = event }
    fun setOnSlide(event: (slideOffset: Float) -> Unit) = apply { _onSlide = event }
    fun setOnStateChanged(event: (newState: Int) -> Unit) = apply { _onStateChanged = event }

    override fun getTheme(): Int = R.style.AppTheme_BottomSheet

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val dialogView = (dialog as? BottomSheetDialog)
        val dialogFrameView = ((dialog as? BottomSheetDialog)?.findViewById<View>(com.google.android.material.R.id.design_bottom_sheet) as? FrameLayout?)

        BottomSheetBehavior.from(dialogFrameView).apply {
            state = initialState
            skipCollapsed = shouldSkipCollapsedState

            setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                    _onSlide(slideOffset)
                }

                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    _onStateChanged(newState)
                }
            })
        }

        if (!enableDim) {
            dialogView?.window?.setDimAmount(0F)
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val baseBottomSheetDialog = object : BottomSheetDialog(requireContext(), theme) {
            override fun onBackPressed() {
                this@ParrotBottomSheetDialogFragment.onBackPressed()
            }
        }

        baseBottomSheetDialog.setOnShowListener {
            onShow(it)
        }

        return baseBottomSheetDialog
    }

    protected open fun onShow(dialogInterface: DialogInterface) {
        _onVisibleChanged(true)
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        _onVisibleChanged(false)
    }

    open fun onBackPressed() {
        dismiss()
    }

}
