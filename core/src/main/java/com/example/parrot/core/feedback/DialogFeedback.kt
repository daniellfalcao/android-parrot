package com.example.parrot.core.feedback

import android.app.Dialog
import com.falcon.turing.core.components.StringWrapper
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import java.lang.ref.WeakReference

class DialogFeedback internal constructor() : Feedback() {

    companion object {

        fun build(builder: DialogFeedback.() -> Unit) = build<DialogFeedback>(builder)
    }

    private var dialog: WeakReference<Dialog>? = null

    var title: StringWrapper? = null
    var cancelable = true

    private var _onDismiss: () -> Unit = { }
    fun setOnDismiss(event: () -> Unit) = apply { _onDismiss = event }

    override fun isShowing(): Boolean {
        return dialog?.get()?.isShowing ?: false
    }

    override fun show() {

        dialog = view?.run {

            MaterialAlertDialogBuilder(context).apply {

                setMessage(message?.invoke(context))

                title?.invoke(context)?.let { setTitle(it) }
                actionMessage?.invoke(context)?.let {
                    setPositiveButton(it) { _, _ -> onActionClick?.invoke() }
                }

                setOnDismissListener { _onDismiss() }

                setCancelable(cancelable)

            }.let { WeakReference(it.show()) }
        }
    }

    override fun dismiss() {
        dialog?.get()?.dismiss()
        dialog = null
    }
}