package com.example.parrot.core.feedback

import android.view.ViewGroup
import com.example.parrot.core.feedback.manager.FeedbackType
import com.example.parrot.core.view.activity.ParrotActivity
import com.example.parrot.core.view.activity.ParrotActivity.Companion.screenKey
import com.falcon.turing.core.components.StringWrapper
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import java.lang.ref.WeakReference

abstract class Feedback : CoroutineScope by CoroutineScope(Dispatchers.Main) {

    companion object {

        @JvmStatic
        protected inline fun <reified T : Feedback> build(builder: T.() -> Unit): T {

            return T::class.java.newInstance().apply(builder).also {

                if (it.message == null) throw IllegalArgumentException("SnackbarFeedback must have a message to work.")
                if (it.actionMessage == null && it.onActionClick != null) throw IllegalArgumentException("OnActionClick only works with a ActionMessage.")
                if (it.actionMessage != null && it.onActionClick == null) throw java.lang.IllegalArgumentException("ActionMessage only works with a ActionClick.")
            }
        }
    }

    var id: Int = 0
    var message: StringWrapper? = null
    var actionMessage: StringWrapper? = null
    var type: FeedbackType = FeedbackType.NEUTRAL

    var hasBeenHandled: Boolean = false
    protected var showingTime: Long = System.currentTimeMillis()

    private var _view: WeakReference<ViewGroup>? = null
    var view: ViewGroup? = null
        set(value) {
            value?.let { _view = WeakReference(value) }
            field = null
        }
        get() = _view?.get()

    private var _onActionClick: WeakReference<(() -> Unit)>? = null
    var onActionClick: (() -> Unit)? = null
        set(value) {
            value?.let { _onActionClick = WeakReference(value) }
            field = null
        }
        get() = _onActionClick?.get()

    @JvmName("setOnActionClick2")
    fun setOnActionClick(event: () -> Unit) {
        onActionClick = event
    }

    fun setAction(actionMessage: StringWrapper, onActionClick: () -> Unit) {
        this.actionMessage = actionMessage
        this.onActionClick = onActionClick
    }

    fun feedbackScreenKey() = (view?.context as? ParrotActivity)?.screenKey() ?: "ERRO"

    open fun show() {
        showingTime = System.currentTimeMillis()
    }

    abstract fun isShowing(): Boolean

    abstract fun dismiss()

}
