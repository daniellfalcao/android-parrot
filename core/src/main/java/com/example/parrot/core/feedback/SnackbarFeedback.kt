package com.example.parrot.core.feedback

import androidx.core.content.ContextCompat
import com.example.parrot.core.R
import com.example.parrot.core.feedback.manager.FeedbackType.*
import com.google.android.material.snackbar.Snackbar
import java.lang.ref.WeakReference

class SnackbarFeedback internal constructor() : Feedback() {

    companion object {

        fun build(builder: SnackbarFeedback.() -> Unit) = build<SnackbarFeedback>(builder)
    }

    private var snackbar: WeakReference<Snackbar>? = null

    var useLongDuration: Boolean = false

    override fun isShowing(): Boolean {
        return snackbar?.get()?.isShown ?: false
    }

    override fun show() {
        super.show()

        snackbar = view?.run {

            Snackbar.make(this, message!!(context), if (useLongDuration) Snackbar.LENGTH_LONG else Snackbar.LENGTH_SHORT).apply {

                val snackbarColor = when (type) {
                    SUCCESS -> R.color.snackbar_success
                    ERROR -> R.color.snackbar_error
                    NEUTRAL -> R.color.snackbar_neutral
                }

                view.setBackgroundColor(ContextCompat.getColor(context, snackbarColor))

                if (actionMessage != null) {
                    setAction(actionMessage!!(context)) { onActionClick?.invoke() }
                }
            }.let { WeakReference(it) }
        }

        snackbar?.get()?.show()
    }

    override fun dismiss() {
        snackbar?.get()?.dismiss()
        snackbar = null
    }

}