package com.example.parrot.core.feedback.manager

enum class FeedbackType { SUCCESS, ERROR, NEUTRAL }