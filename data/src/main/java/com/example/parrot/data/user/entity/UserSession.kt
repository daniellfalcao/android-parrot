package com.example.parrot.data.user.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey

const val SESSION_ID = 0

@Entity(
    tableName = "user_session",
    foreignKeys = [
        ForeignKey(
            entity = UserEntity::class,
            parentColumns = ["id"],
            childColumns = ["user_id"],
            onDelete = CASCADE
        )]
)
data class UserSession(
    var token: String,
    @ColumnInfo(name = "user_id") var userID: Int,
    @PrimaryKey var id: Int = SESSION_ID
)
