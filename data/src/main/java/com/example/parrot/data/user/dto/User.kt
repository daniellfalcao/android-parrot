package com.example.parrot.data.user.dto

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("nome") var name: String? = null,
    @SerializedName("username") var username: String? = null,
    @SerializedName("email") var email: String? = null,
    @SerializedName("foto") var photoURL: String? = null,
    @SerializedName("amigos") var friends: List<User>? = null,
    @SerializedName("token") var token: String? = null,
    @SerializedName("password") var password: String? = null
)