package com.example.parrot.data.user.entity

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(
    tableName = "user_friend",
    primaryKeys = ["user_id", "friend_id"]
)
data class UserFriendEntity(
    @ColumnInfo(name = "user_id") var userID: Int,
    @ColumnInfo(name = "friend_id") var friendID: Int
)