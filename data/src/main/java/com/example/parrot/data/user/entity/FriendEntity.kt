package com.example.parrot.data.user.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "friend")
data class FriendEntity(
    @PrimaryKey var id: Int,
    var name: String,
    var username: String,
    var email: String,
    @ColumnInfo(name = "photo_url") var photoURL: String
){

    constructor() : this(0, "", "", "", "")
}

/**
 * DSL to create a friendEntity
 * */
fun friendEntity(builder: FriendEntity.() -> Unit): FriendEntity {
    return FriendEntity().apply(builder)
}