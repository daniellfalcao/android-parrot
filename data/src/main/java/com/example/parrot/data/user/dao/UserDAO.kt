package com.example.parrot.data.user.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import com.example.parrot.data.user.entity.UserEntity

@Dao
abstract class UserDAO {

    @Insert(onConflict = REPLACE)
    abstract suspend fun add(user: UserEntity)

    @Query("SELECT * FROM user WHERE id = :id")
    abstract suspend fun get(id: Int): UserEntity

    @Query("SELECT * FROM user")
    abstract suspend fun getAll(): List<UserEntity>



}