package com.example.parrot.data.user.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.parrot.data.user.entity.SESSION_ID
import com.example.parrot.data.user.entity.UserSession

@Dao
abstract class SessionDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract suspend fun add(session: UserSession)

    @Query("DELETE FROM user_session")
    abstract suspend fun delete()

    @Query("SELECT * FROM user_session WHERE id = :sessionID")
    abstract suspend fun get(sessionID: Int = SESSION_ID): UserSession

    @Query("SELECT * FROM user_session WHERE id = :sessionID")
    abstract fun getObservable(sessionID: Int = SESSION_ID): LiveData<UserSession>

}