package com.example.parrot.data.user.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserEntity(
    @PrimaryKey var id: Int,
    var name: String,
    var username: String,
    var email: String,
    @ColumnInfo(name = "photo_url") var photoURL: String
) {

    constructor() : this(0, "", "", "", "")
}

/**
 * DSL to create a userEntity
 * */
fun userEntity(builder: UserEntity.() -> Unit): UserEntity {
    return UserEntity().apply(builder)
}