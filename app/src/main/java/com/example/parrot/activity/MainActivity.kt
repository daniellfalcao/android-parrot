package com.example.parrot.activity

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.parrot.R
import com.example.parrot.application.ServiceProvider
import com.example.parrot.core.feedback.SnackbarFeedback
import com.example.parrot.core.view.activity.ParrotActivity
import com.example.parrot.databinding.ActivityMainBinding
import com.example.parrot.uimodel.LoginUIModel
import com.example.parrot.viewmodel.LoginViewModel
import com.falcon.turing.lifecycle.getViewModel
import com.falcon.turing.lifecycle.util.observeEvent

class MainActivity : ParrotActivity() {

    private lateinit var binder: ActivityMainBinding
    private lateinit var uiModel: LoginUIModel
    private lateinit var viewModel: LoginViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binder = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewModel = getViewModel { LoginViewModel(ServiceProvider.getSignInUseCase(application)) }
        uiModel = LoginUIModel(binder, this)
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)

        with(uiModel) {

            setup()
            setOnSignInClicked {
                viewModel.attemptLogin(email, password)
            }
            setOnSignUpClicked {
                println("clickei no signup")
            }
        }

        with(viewModel) {

            onInvalidEmail.observeEvent(this@MainActivity) {
                uiModel.showEmailError(it)
            }

            onInvalidPassword.observeEvent(this@MainActivity) {
                uiModel.showPasswordError(it)
            }

            onError.observeEvent(this@MainActivity) {
                uiModel.showError(it)
            }
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        uiModel.saveState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        uiModel.restoreState(savedInstanceState)
    }

}