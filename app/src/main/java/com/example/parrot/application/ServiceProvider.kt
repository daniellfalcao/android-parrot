package com.example.parrot.application

import android.app.Application
import com.example.parrot.domain.database.AppDatabase
import com.example.parrot.domain.repository.user.UserLocalDataSource
import com.example.parrot.domain.repository.user.UserRemoteDataSource
import com.example.parrot.domain.repository.user.UserRepository
import com.example.parrot.domain.usecase.user.SignInUseCase

// TODO: Usar Koin
object ServiceProvider {


    fun getSignInUseCase(application: Application): SignInUseCase {
        return SignInUseCase(
            UserRepository(
                UserLocalDataSource(
                    AppDatabase.getInstance(application).userDAO,
                    AppDatabase.getInstance(application).sessionDAO
                ),
                UserRemoteDataSource()
            )
        )
    }
}