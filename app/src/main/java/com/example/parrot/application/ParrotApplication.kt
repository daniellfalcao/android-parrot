package com.example.parrot.application

import android.app.Application
import com.example.parrot.BuildConfig
import com.example.parrot.core.feedback.manager.FeedbackManager
import com.example.parrot.domain.database.AppDatabase
import com.facebook.stetho.Stetho
import timber.log.Timber
import timber.log.Timber.DebugTree

class ParrotApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        setupFeedbackManager()
        setupRoomDatabase()
        setupTimber()
        setupStetho()
    }

    fun setupRoomDatabase() {
        AppDatabase.getInstance(this)
    }

    fun setupFeedbackManager() {
        FeedbackManager.getInstance(this)
    }

    fun setupTimber() {
        if (BuildConfig.DEBUG) Timber.plant(DebugTree())
    }

    fun setupStetho() {
        Stetho.initializeWithDefaults(this)
    }

}