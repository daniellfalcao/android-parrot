package com.example.parrot.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.parrot.domain.Result
import com.example.parrot.domain.api.RemoteException
import com.example.parrot.domain.usecase.user.SignInUseCase
import com.example.parrot.domain.usecase.user.SignInUseCase.SignInValidationException.SignInError.EMAIL
import com.example.parrot.domain.usecase.user.SignInUseCase.SignInValidationException.SignInError.PASSWORD
import com.falcon.turing.core.components.StringWrapper
import com.falcon.turing.lifecycle.util.Event
import kotlinx.coroutines.launch

class LoginViewModel(private val signInUseCase: SignInUseCase) : ViewModel() {

    val onInvalidEmail = MutableLiveData<Event.Data<StringWrapper>>()
    val onInvalidPassword = MutableLiveData<Event.Data<StringWrapper>>()

    val onError = MutableLiveData<Event.Data<StringWrapper>>()

    fun attemptLogin(email: String, password: String) {

        viewModelScope.launch {

            when(val result = signInUseCase.execute(SignInUseCase.Param(email = email, password = password))) {

                is Result.Success -> {
                    println("iei sucesso")
                }

                is Result.Failure -> {

                    when(val exception = result.error) {

                        is RemoteException -> {
                            onError.value = Event.Data(exception.errorMessage)
                        }

                        is SignInUseCase.SignInValidationException -> {

                            // handle fields exception
                            exception.errors.forEach {
                                when(it) {
                                    EMAIL -> onInvalidEmail.value = Event.Data(StringWrapper("Email inválido"))
                                    PASSWORD -> onInvalidPassword.value = Event.Data(StringWrapper("A senha precisa ter 8 dígitos"))
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}