package com.example.parrot.uimodel

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.widget.TextView
import com.example.parrot.core.feedback.SnackbarFeedback
import com.example.parrot.core.feedback.manager.FeedbackType
import com.example.parrot.core.feedback.manager.dispatchToManager
import com.example.parrot.core.uimodel.UIModel
import com.example.parrot.databinding.ActivityMainBinding
import com.falcon.turing.core.components.StringWrapper

class LoginUIModel(binding: ActivityMainBinding, context: Context) : UIModel<ActivityMainBinding>(binding, context) {

    private var _onClickSignIn = { }
    private var _onClickSignUp = { }

    var email: String
        set(value) = run { binding.textInputEmail.editText?.setText(value, TextView.BufferType.EDITABLE) }
        get() = binding.textInputEmail.editText?.text?.toString() ?: ""

    var password: String
        set(value) = run { binding.textInputPassword.editText?.setText(value, TextView.BufferType.EDITABLE) }
        get() = binding.textInputPassword.editText?.text?.toString() ?: ""

    var enableButtonLogin = true
        set (value) = run { binding.buttonSignIn.isEnabled = value }

    override fun setup() {

        val clearErrorWatcher = object : TextWatcher {
            override fun afterTextChanged(p0: Editable?) {
            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                if (binding.textInputEmail.editText?.hasFocus() == true) {
                    binding.textInputEmail.error = null
                }

                if (binding.textInputPassword.editText?.hasFocus() == true) {
                    binding.textInputEmail.error = null
                }
            }
        }

        binding.textInputEmail.editText?.addTextChangedListener(clearErrorWatcher)
        binding.textInputPassword.editText?.addTextChangedListener(clearErrorWatcher)
    }

    fun showEmailError(error: StringWrapper) {

        if (binding.textInputEmail.error?.toString() ?: "" != error(context)) {
            binding.textInputEmail.error = error(context)
        }
    }

    fun showPasswordError(error: StringWrapper) {

        if (binding.textInputPassword.error?.toString() ?: "" != error(context)) {
            binding.textInputPassword.error = error(context)
        }
    }

    override fun showError(error: StringWrapper) {

        SnackbarFeedback.build {
            view = binding.layoutContainer
            message = error
            type = FeedbackType.ERROR
        }.dispatchToManager()
    }

    fun setOnSignInClicked(event: () -> Unit) = apply {
        _onClickSignIn = event
        binding.buttonSignIn.setOnClickListener { _onClickSignIn() }
    }

    fun setOnSignUpClicked(event: () -> Unit) = apply {
        _onClickSignUp = event
        binding.buttonSignUp.setOnClickListener { _onClickSignUp() }
    }

}